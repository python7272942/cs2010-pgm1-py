#Ticket Prices
adultTicketPrice = float(10.00)
childTicketPrice = float(6.00)
adultTicketsSold = float(0)
childTicketsSold = float(0)
grossTicketSales = float(0.0)

movieName = "a"

#Get Inputs
movieName = input("Enter the movie name:\t\t\t")
adultTicketsSold = input("How many adult tickets were sold:\t")
childTicketsSold = input("How many child tickets were sold:\t")

#Calculate Gross Tickets Sales
grossTicketSales = float(adultTicketsSold) * float(adultTicketPrice) + float(childTicketsSold) * float(childTicketPrice)

#Print Results
print("\nGross Ticket Sales:\t\t\t${0}".format(grossTicketSales))
print("Theatre Net Profit:\t\t\t${0}".format(grossTicketSales / 5))
print("Amount Paid to Distributor:\t\t${0}".format(grossTicketSales * .8))
